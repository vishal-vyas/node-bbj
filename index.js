const JDBC = require('jdbc');
const config = require('./config');
let jinst = require('jdbc/lib/jinst');

if (!jinst.isJvmCreated()) {
    jinst.addOption("-Xrs");
    jinst.setupClasspath(['./drivers/BBjJDBC.jar']);
}

let BBJdb = new JDBC(config);
//initialize
BBJdb.initialize(function (err) {
    if (err) {
        console.log(err);
    }
});
BBJdb.reserve(function (err, connObj) {
    if (connObj) {
        console.log("Using connection: " + connObj.uuid);
        let conn = connObj.conn;
        // Query the database.
        // Select statement example.
        conn.createStatement(function (err, statement) {
            //Execute a query
            if (err) {
                return console.log('st', err);
            }
            statement.executeQuery("SELECT * FROM CUSTOMER_DATA",
                function (err, resultset) {
                    if (err) {
                        return console.log('ex', err);
                    }
                    resultset.toObjArray(function (err, results) {
                        //Printing name of customer
                        if (results.length > 0) {
                            console.log("Customer Name " + results[0].CUST_NAME);
                        }
                    });
                });
        });
    }
});